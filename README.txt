Simplistic Silent ScreenCast (GIF)

Dependencies
 Either
 - maim and bc and imagemagick's convert for method 1
 or
 - ffmpeg for method 2

Optional dependencies
 - slop for interactive region selection
 - gifsicle for optimisation (only method 2)

Options
 -g str  pass geometry string instead of interactively selecting it

 -w int  wait int seconds before starting (default: 2)

 -f int  set frames per second (default: 10)

 -d int  set duration of recording in seconds instead of quitting interactively

 -o str  define output file (default: sssc20200420082424.gif)

 -m 1|2  define method to use:
           1 take screenshots with maim and stitch them together to a looping
             GIF. Try to record the exact number of frames per second, but if
             the screenshoting takes too long, don't try to keep up.
             this method is more resource intensive but produces _much_ smaller
             files.
           2 ffmpeg for both screencast and GIF conversion. If gifsicle is
             installed, optimise. Even optimised, filesize is 5x - 20x larger
             than with method 1.

         currently defaults to method 1.

 -v      don't convert intermediate video to gif. only applies to method 2

 -h      this text
